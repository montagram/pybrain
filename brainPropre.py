from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader
#from pybrain.tools.customxml import NetworkWriter, NetworkReader
from PIL import Image
import numpy as np
import pylab as pl

TAILLE = 70
SPLIT = 0.25
NB_ITERATION = 8
ERROR_THRESHOLD = 0.80

INDEX_MP = 0
INDEX_CER = 1
INDEX_TC = 2
NB_IM_MP = 242
NB_IM_CER = 165
NB_IM_TC = 892
NB_IM = NB_IM_MP + NB_IM_CER + NB_IM_TC

#Put image in grayscale, rescale it, convert it as a vector
def imageProcess(im):
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	return imVect

#Load images of Huayna Picchu from folder IM/MP/.. , and add it to dataset
def loadImMachuPicchu(alldata, nbImages = NB_IM_MP):
	for imageMachuPicchu in xrange(nbImages):
		im = Image.open(open("Im/MP/mp" + str(imageMachuPicchu+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [INDEX_MP])
	return

#Load images of Mt CERVIN from folder IM/MC/.. , and add it to dataset
def loadImCervin(alldata, nbImages = NB_IM_CER):
	for imageCervin in xrange(nbImages):	
		im = Image.open(open("Im/MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [INDEX_CER])
	return

#Load images of Meije from folder IM/TC/.. , and add it to dataset
def loadImTc(alldata, nbImages = NB_IM_TC):
	for imageTc in xrange(nbImages):
		im = Image.open(open("Im/TC/tc" + str(imageTc+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [INDEX_TC])
	return

#Load the image we want, given the name and the class (MP, MC or TC)
def loadOneIm(imgName,imgClass):
		im = Image.open(open("Im/"+imgClass+"/"+imgName+".jpg", 'rb'))
		imVect =imageProcess(im)
		return imVect

#Load all images of dataset
def makedataset():

	alldata = ClassificationDataSet(TAILLE*TAILLE, 1, nb_classes=3, class_labels=['Huayna Picchu', 'Cervin', 'TC Building'])
	loadImCervin(alldata)
	tstdata, trndata = alldata.splitWithProportion( SPLIT )
	loadImTc(alldata)
	tstdata, trndata = alldata.splitWithProportion( SPLIT )
	loadImMachuPicchu(alldata)
	tstdata, trndata = alldata.splitWithProportion( SPLIT )
	
	trndata._convertToOneOfMany( )
	tstdata._convertToOneOfMany( )
	alldata._convertToOneOfMany( )
	
	return (alldata, trndata, tstdata)

#Train network and save matrix
def training(trndata, tstdata):

	print "Number of training patterns: ", len(trndata)
	print "Input and output dimensions: ", trndata.indim, trndata.outdim
	print "First sample (input, target, class):"
	print trndata['input'][0], trndata['target'][0], trndata['class'][0]
	print "Photos/class", trndata.calculateStatistics()

	trnResultTab = []
	tstResultTab = []

	#Define network(dimension of input layer, dimension of first hiddenlayer, dimension of output layer)	
	fnn = buildNetwork(trndata.indim, 750, 750, trndata.outdim, outclass=SoftmaxLayer)
	trainer = BackpropTrainer(fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01) #define trainer

	#THIRD TRAINING METHOD
	for i in range(NB_ITERATION):
		trainer.train()
		trnresult = percentError( trainer.testOnClassData(), trndata['class'] )
		tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
		print "epoch: %4d" % trainer.totalepochs, "  train error: %5.2f%%" % trnresult, "  test error: %5.2f%%" % tstresult
		
		trnResultTab.append(trnresult)
		tstResultTab.append(tstresult)

	NetworkWriter.writeToFile(fnn, 'network.xml')

	return (trnResultTab,tstResultTab, fnn)

#Given a THRESHOLD, test if the result given by neural net is equal to the true label or not
#Compute error and unknown (unrecognized)
def testOfValidity(result,imgName,imgClass,error,unknown):

	percent = result*100

	if result[INDEX_CER] >= ERROR_THRESHOLD :
		print "This image "+imgName+" is the Cervin (accuracy %f%%)\n" % percent[INDEX_CER]
		if imgClass != "MC":
			error+=1
	elif result[INDEX_TC] >= ERROR_THRESHOLD:
		print "This image "+imgName+" is the TC Building (accuracy %f%%)\n" % percent[INDEX_TC]
		if imgClass != "TC":
			error+=1
	elif result[INDEX_MP] >= ERROR_THRESHOLD:
		print "This image "+imgName+" is the Huayna Picchu (accuracy %f%%)\n" % percent[INDEX_MP]
		if imgClass != "MP":
			error+=1
	elif result[INDEX_TC] < ERROR_THRESHOLD and result[INDEX_CER] < ERROR_THRESHOLD and result[INDEX_MP] < ERROR_THRESHOLD:
		print "Couldn't recognize your image "+imgName+" (Cervin : %f%%, Huayna Picchu : %f%%, TC Building : %f%%)\n" %(percent[INDEX_CER], percent[INDEX_MP], percent[INDEX_TC])
		unknown+=1
	
	return (error,unknown)

#Print statistics of the neural network
def statistics(error, unknown, yCervin, yTc, yPicchu):
	
	reco = NB_IM - unknown
	errorRate = error * 100 / reco
	print "\n\n\t\t\t-------- TRAINING RESULTS -------\n"
	print "Dataset is composed of : %d images \nErrors : %d" %(NB_IM, error)
	print "Non recognized images : %d \nError rate (without unrecognized) : %f%%\n\n" % (unknown, errorRate)

	print "\t\t Mont Cervin \t\t TC Building\t\tHuayna Picchu"
	print "Min\t\t ",np.around(min(yCervin),decimals=2),"%\t\t ",np.around(min(yTc),decimals=2),"%\t\t ",np.around(min(yPicchu),decimals=2),"%\t\t "
	print "Mean\t\t ",np.around(np.mean(yCervin),decimals=2),"%\t\t ",np.around(np.mean(yTc),decimals=2),"%\t\t ",np.around(np.mean(yPicchu),decimals=2),"%\t\t"
	print "Max\t\t ",np.around(max(yCervin),decimals=2),"%\t\t ",np.around(max(yTc),decimals=2),"%\t\t ",np.around(max(yPicchu),decimals=2),"%\t\t"

#Plot curves that shows training results for trained photos
def plotTraining(xC,yC,xT,yT,xP,yP):
	pl.figure(1)
	pl.plot(xC,yC,label='Mont Cervin')
	pl.plot(xT,yT,label='TC Building')
	pl.plot(xP,yP,label='Huayna Picchu')
	pl.ylabel('Validation rate')
	pl.xlabel('Pictures of mountains')
	pl.legend()
	pl.show()

#Plot curves that test if there overfitting or not
def plotOverfitting(trnresult,tstresult):
	pl.figure(1)
	x = np.linspace(0, NB_ITERATION-1, NB_ITERATION)
	pl.plot(x,trnresult,label='Train results')
	pl.plot(x,tstresult,label='Tests results')
	pl.ylabel("Error rate")
	pl.xlabel("NNumber of iteration for the training")
	pl.legend()
	pl.show()

#Test all images
def testAll(error,unknown,alldata, fnn):
	fnn = NetworkReader.readFrom('network.xml')
	
	targetList=[]
	result = fnn.activateOnDataset(alldata) #Test all dataset
	
	xCervin = np.linspace(0,NB_IM_CER-1,NB_IM_CER)
	yCervin = []
	xTc = np.linspace(0,NB_IM_TC-1,NB_IM_TC)
	yTc = []
	xPicchu = np.linspace(0,NB_IM_MP-1,NB_IM_MP)
	yPicchu = []


	for inpt, target in alldata:
		targetList.append(target) #Get the true target of each image from alldata

	for i in xrange(NB_IM_CER):
		(error, unknown) = testOfValidity(result[i],"mc"+str(i+1),"MC",error,unknown)
		z=result[i]
		yCervin.append(z[1]*100)
	for i in xrange(NB_IM_TC):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER],"tc"+str(i+1),"TC",error,unknown)
		z=result[i+NB_IM_CER]
		yTc.append(z[2]*100)
	for i in xrange(NB_IM_MP):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER+NB_IM_TC],"mp"+str(i+1),"MP",error,unknown)
		z=result[i+NB_IM_CER+NB_IM_TC]
		yPicchu.append(z[0]*100)

	statistics(error, unknown,yCervin,yTc,yPicchu)
	plotTraining(xCervin,yCervin,xTc,yTc,xPicchu,yPicchu)

#Test for one image	
def testOneIm(imgName,imgClass,error,unknown):
	fnn = NetworkReader.readFrom('network.xml') 
	image = loadOneIm(imgName,imgClass)
	result = fnn.activate(image)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)

#Test for one unknown image (from user)
def testOneImUnknown(imgName):

	fnn = NetworkReader.readFrom('network.xml') 
	im = Image.open(open("Im/TC/tc800.jpg", 'rb'))
	image = imageProcess(im)
	result = fnn.activate(image)
	resultList=[]
	for i in result:
		resultList.append(i)

	mountain = max(resultList)
	mountainIndex = resultList.index(mountain)
	mountainPercent = mountain*100

	if mountain >= ERROR_THRESHOLD :
		print "Votre photo represente",
		if mountainIndex == INDEX_TC :
			print "le Batiment TC (fiabilite : ", mountainPercent,"% )."
		elif mountainIndex == INDEX_CER :
			print "le Mont Cervin (fiabilite : ", mountainPercent,"% )."
		elif mountainIndex == INDEX_MP :
			print "le Machu Picchu (fiabilite : ", mountainPercent,"% )."
	else:
		print "Nous n'avons pas pu reconnaitre votre montagne. Voici les resultats : "
		print "Cervin : %f%%, Huayna Picchu : %f%%, Batiment TC : %f%%" %(mountainPercent[INDEX_CER], mountainPercent[INDEX_MP], mountainPercent[INDEX_TC])

def main():
	
	#Initialization
	error = 0
	unknown = 0

	(alldata, trndata,tstdata) = makedataset()
	(trnResultTab, tstResultTab, fnn) = training(trndata,tstdata)
	testAll(error,unknown,alldata,fnn)
	plotOverfitting(trnResultTab,tstResultTab)
	#testOneIm("mc1","MC",error,unknown)
	#testOneImUnknown("tc800")

if __name__ == '__main__':

	main()
