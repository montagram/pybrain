###
# xor.py
#
# author: Kristina Striegnitz
#
# version: 3/3/2010
#
# Simple example of training a neural network calculating XOR using
# the pybrain package.
###

# Traitement d'images
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised import BackpropTrainer

TAILLE = 5
NB_IM_CER = 77
NB_IM_OTHERS = 32

data = []

# premier param = taille des entrees
dataset = SupervisedDataSet((TAILLE*TAILLE),1)


def loadImCervin(nbImages = NB_IM_CER):
	for imageCervin in xrange(nbImages):
		datBuf = []
		im = Image.open(open("MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((TAILLE,TAILLE))
		matrix = np.asarray(im)
		imVect = matrix.flatten().tolist()
		dataset.addSample(imVect,[1])		
		
	return [dataset]	

def loadImOthers( nbImages = NB_IM_OTHERS):
	for imageAutre in xrange(nbImages):
		datBuf = []
		im = Image.open(open("PM/" + str(imageAutre+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((TAILLE,TAILLE))
		matrix = np.asarray(im)
		imVect = matrix.flatten().tolist()
		dataset.addSample(imVect,[0])
		
	return [dataset]




def make_dataset():

    """
    Creates a set of training data.
    """
    data = SupervisedDataSet(2,1)

    data.addSample([1,1],[0])
    data.addSample([1,0],[1])
    data.addSample([0,1],[1])
    data.addSample([0,0],[0])

    return data


def training(d):
    """
    Builds a network and trains it.
    """
    n = buildNetwork(d.indim, 10,d.outdim,recurrent=True)
    t = BackpropTrainer(n, d, learningrate = 0.01, momentum = 0.99, verbose = True)
    for epoch in range(0,1000):
        t.train()
    return t


def test(trained):
    """
    Builds a new test dataset and tests the trained network on it.
    """
    testdata = SupervisedDataSet(TAILLE*TAILLE,1)

    for imageCervin in xrange(5):
	im = Image.open(open("MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	testdata.addSample(imVect,[1])

    for imageAutre in xrange(5):
	datBuf = []
	im = Image.open(open("PM/" + str(imageAutre+1) + ".jpg", 'rb'))
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	testdata.addSample(imVect,[0])

    #testdata.addSample([1,1],[0])
    #testdata.addSample([1,0],[1])
    #testdata.addSample([0,1],[1])
    #testdata.addSample([0,0],[0])
    trained.testOnData(testdata, verbose= True)


def run():
    """
    Use this function to run build, train, and test your neural network.
    """
    trainingdata = make_dataset()
    trained = training(trainingdata)
    test(trained)
    """
    print '0,0->', net.activate([0,0])
    print '0,1->', net.activate([0,1])
    print '1,0->', net.activate([1,0])
    print '1,1->', net.activate([1,1])
    """


if __name__ == '__main__':
	trainingdata = SupervisedDataSet((TAILLE*TAILLE),1)
	loadImCervin()
	trainingdata = loadImOthers()
	#print trainingdata.indim
	trained = training(dataset)
	test(trained)
	
