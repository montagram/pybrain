from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer
from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal
#from pybrain.tools.xml.networkwriter import NetworkWriter
#from pybrain.tools.xml.networkreader import NetworkReader
from pybrain.tools.validation import testOnSequenceData, ModuleValidator, Validator, CrossValidator

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

TAILLE = 50
NB_IM_CER = 76
NB_IM_OTHERS = 99
NB_IM_MEIJE = 77
NB_IM_MP = 59
NB_IM = NB_IM_CER + NB_IM_OTHERS + NB_IM_MEIJE + NB_IM_MP
ERROR_THRESHOLD = 0.80

#Put image in grayscale, rescale it, convert it as a vector
def imageProcess(im):
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	return imVect

#Load images of Mt CERVIN from folder IM/MC/.. , and add it to dataset
def loadImCervin(alldata, nbImages = NB_IM_CER):

	for imageCervin in xrange(nbImages):	
		im = Image.open(open("Im/MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [1])
	return

#Load random images from folder IM/PM/.. , and add it to dataset
def loadImOthers(alldata, nbImages = NB_IM_OTHERS):

	for imageAutre in xrange(nbImages):
		im = Image.open(open("Im/PM/" + str(imageAutre+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [0])
	return

#Load images of Meije from folder IM/MEIJE/.. , and add it to dataset
def loadImMeije(alldata, nbImages = NB_IM_MEIJE):

	for imageMeije in xrange(nbImages):
		im = Image.open(open("Im/MEIJE/meije" + str(imageMeije+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [2])
	return


#Load images of Huayna Picchu from folder IM/MP/.. , and add it to dataset
def loadImMachuPicchu(alldata, nbImages = NB_IM_MP):

	for imageMachuPicchu in xrange(nbImages):
		im = Image.open(open("Im/MP/mp" + str(imageMachuPicchu+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [3])
	return

#Load the image we want, given the name and the class (MC or PM)
def loadOneIm(imgName,imgClass):
		im = Image.open(open("Im/"+imgClass+"/"+imgName+".jpg", 'rb'))
		imVect =imageProcess(im)
		return imVect

#Given a THRESHOLD, test if the result given by neural net is equal to the true label or not
#Compute error and unknown (unrecognized)
def testOfValidity(result,imgName,imgClass,error,unknown):
	
	percent = result*100

	if result[1] >= ERROR_THRESHOLD :
    	    print "This image "+imgName+" is the Cervin (accuracy %f%%)\n" % percent[1]
	    if imgClass != "MC":
	        error+=1
	elif result[0] >= ERROR_THRESHOLD:
            print "This image "+imgName+" is not a mountain (accuracy %f%%)\n" % percent[0]
	    if imgClass != "PM":
	        error+=1
	elif result[2] >= ERROR_THRESHOLD:
            print "This image "+imgName+" is the Meije (accuracy %f%%)\n" % percent[2]
	    if imgClass != "MEIJE":
	        error+=1
	elif result[3] >= ERROR_THRESHOLD:
            print "This image "+imgName+" is the Huayna Picchu (accuracy %f%%)\n" % percent[3]
	    if imgClass != "MP":
	        error+=1
	elif result[0] < ERROR_THRESHOLD and result[1] < ERROR_THRESHOLD and result[2] < ERROR_THRESHOLD and result[3] < ERROR_THRESHOLD:
            print "Couldn't recognize your image "+imgName+" (Cervin : %f%%, Meije : %f%%, Huayna Picchu : %f%%, Other : %f%%)\n" %(percent[1], percent[2], percent[3], percent[0])
	    unknown+=1
	
	return (error,unknown)

#Test recognition for one given image
def recognizeOneIm(network,imgName,imgClass,error,unknown):
	
	image = loadOneIm(imgName,imgClass)
	result = network.activate(image)
	return (result,imgName,imgClass)

#Test all dataset
def recognizeAllIm(fnn, alldata, error,unknown):

	targetList=[]
	result = fnn.activateOnDataset(alldata) #Test all dataset
	
	for inpt, target in alldata:
	 	targetList.append(target) #Get the true target of each image from alldata

	for i in xrange(NB_IM_CER):
		(error, unknown) = testOfValidity(result[i],"mc"+str(i+1),"MC",error,unknown)
	
	for i in xrange(NB_IM_OTHERS):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER],str(i+1),"PM",error,unknown)

	for i in xrange(NB_IM_MEIJE):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER+NB_IM_OTHERS],"meije"+str(i+1),"MEIJE",error,unknown)

	for i in xrange(NB_IM_MP):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER+NB_IM_OTHERS+NB_IM_MEIJE],"mp"+str(i+1),"MP",error,unknown)

	return (error, unknown)


#Print statistics of the neural network
def statistics(error, unknown):
	
	reco = NB_IM - unknown
	errorRate = error * 100 / reco
	print "\n\n-------- RESULTS -------"
	print "Dataset is composed of : %d images \nErrors : %d" %(NB_IM, error)
	print "Non recognized images : %d \nError rate (without unrecognized) : %f%%\n\n" % (unknown, errorRate)


def main():
	
	#Make dataset
	alldata = ClassificationDataSet(TAILLE*TAILLE, 1, nb_classes=4, class_labels=['PasMontagne', 'Cervin', 'Meije', 'Huayna Picchu'])
	loadImCervin(alldata)
	loadImOthers(alldata)
	loadImMeije(alldata)
	loadImMachuPicchu(alldata)
	trndata=alldata
	trndata._convertToOneOfMany( )

	print "Number of training patterns: ", len(trndata)
	print "Input and output dimensions: ", trndata.indim, trndata.outdim
	print "First sample (input, target, class):"
	print trndata['input'][0], trndata['target'][0], trndata['class'][0]

	#Define network(dimension of input layer, dimension of first hiddenlayer, dimension of output layer)	
	fnn = buildNetwork(trndata.indim, 2000, 2000, trndata.outdim, outclass=SoftmaxLayer)
	#fnn = NetworkReader.readFrom('network.xml') 
	trainer = BackpropTrainer(fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01) #define trainer
	
	#FIRST TRAINING METHOD
	# for i in range(5):
	# 	trainer.trainEpochs( 5 )
	# 	trnresult = percentError( trainer.testOnClassData(), trndata['class'] )
	# 	tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
	# 	print "epoch: %4d" % trainer.totalepochs, "  train error: %5.2f%%" % trnresult, "  test error: %5.2f%%" % tstresult
	
	#SECOND TRAINING METHOD
	# trainer.trainUntilConvergence()
	# trnresult = percentError( trainer.testOnClassData(), trndata['class'] )
	# tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
	# print "  train error: %5.2f%%" % trnresult, "  test error: %5.2f%%" % tstresult
	
	#THIRD TRAINING METHOD
	for i in range(12):
		trainer.train()
	# 	cv = CrossValidator(trainer,trndata, n_folds=5, valfunc=ModuleValidator.MSE) #Returns the mean squared error 
	# 	print "MSE %f @ %i" %( cv.validate(), i )

	#trainer.testOnData(alldata, verbose= True)
	#NetworkWriter.writeToFile(fnn, 'network.xml')
	return (fnn,alldata)

if __name__ == '__main__':
	
	#Initialization
	error = 0
	unknown = 0

    	(fnn,alldata) = main()

	#Test for all dataset
	(error, unknown) = recognizeAllIm(fnn, alldata, error, unknown)
	statistics(error, unknown)

	#Test for one image
	(result,imgName,imgClass) = recognizeOneIm(fnn,"mc77","MC",error,unknown)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)

	#Test for one image
	(result,imgName,imgClass) = recognizeOneIm(fnn,"100","PM",error,unknown)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)

	#Test for one image
	(result,imgName,imgClass) = recognizeOneIm(fnn,"meije78","MEIJE",error,unknown)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)

	#Test for one image
	(result,imgName,imgClass) = recognizeOneIm(fnn,"mp60","MP",error,unknown)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)