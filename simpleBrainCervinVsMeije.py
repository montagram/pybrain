from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer
from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal
#from pybrain.tools.xml.networkwriter import NetworkWriter
#from pybrain.tools.xml.networkreader import NetworkReader
from pybrain.tools.validation import testOnSequenceData, ModuleValidator, Validator, CrossValidator

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

TAILLE = 50
NB_IM_CER = 77
NB_IM_MEIJE = 78
NB_IM = NB_IM_CER + NB_IM_MEIJE
ERROR_THRESHOLD = 0.80

#Put image in grayscale, rescale it, convert it as a vector
def imageProcess(im):
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	return imVect

#Load images of Mt CERVIN from folder IM/MC/.. , and add it to dataset
def loadImCervin(alldata, nbImages = NB_IM_CER):

	for imageCervin in xrange(nbImages):	
		im = Image.open(open("Im/MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [1])
	return

#Load RANDOM images from folder IM/MEIJE/.. , and add it to dataset
def loadImMeije(alldata, nbImages = NB_IM_MEIJE):

	for imageMeije in xrange(nbImages):
		im = Image.open(open("Im/MEIJE/meije" + str(imageMeije+1) + ".jpg", 'rb'))
		imVect =imageProcess(im)
		alldata.addSample(imVect, [0])
	return

#Load ONE image of Cervin 
"""
def loadOneImCervin():

		im = Image.open(open("Im/MC/mc1.jpg", 'rb'))
		imVect =imageProcess(im)
		return imVect
"""

#Load ONE random image
"""
def loadOneImOther():
		
		im = Image.open(open("Im/PM/1.jpg", 'rb'))
		imVect =imageProcess(im)
		return imVect
"""

#Load the image we want, given the name and the class (MC or MEIJE)
def loadOneIm(imgName,imgClass):
		im = Image.open(open("Im/"+imgClass+"/"+imgName+".jpg", 'rb'))
		imVect =imageProcess(im)
		return imVect

#Given a THRESHOLD, test if the result given by neural net is equal to the true label or not
#Compute error and unknown (unrecognized)
def testOfValidity(result,imgName,imgClass,error,unknown):
	
	percent = result*100

	if result[1] >= ERROR_THRESHOLD :
    	    print "This image "+imgName+" is the Cervin (accuracy %f%%)\n" % percent[1]
	    if imgClass != "MC":
	        error+=1
	elif result[0] >= ERROR_THRESHOLD:
            print "This image "+imgName+" is the Meije (accuracy %f%%)\n" % percent[0]
	    if imgClass != "MEIJE":
	        error+=1
	elif result[0] < ERROR_THRESHOLD and result[1] < ERROR_THRESHOLD:
            print "Couldn't recognize your image "+imgName+" (Cervin : %f%%, Meije : %f%%)\n" %(percent[1], percent[0])
	    unknown+=1
	
	return (error,unknown)

#Test recognition for one given image
def recognizeOneIm(network,imgName,imgClass,error,unknown):
	
	image = loadOneIm(imgName,imgClass)
	result = network.activate(image)
	return (result,imgName,imgClass)

#Test all dataset
def recognizeAllIm(fnn, alldata, error,unknown):

	targetList=[]
	result = fnn.activateOnDataset(alldata) #Test all dataset
	
	for inpt, target in alldata:
	 	targetList.append(target) #Get the true target of each image from alldata

	for i in xrange(NB_IM_CER):
		(error, unknown) = testOfValidity(result[i],"mc"+str(i+1),"MC",error,unknown)
	
	for i in xrange(NB_IM_MEIJE):
		(error, unknown) = testOfValidity(result[i+NB_IM_CER],"meije"+str(i+1),"MEIJE",error,unknown)

	return (error, unknown)


#Print statistics of the neural network
def statistics(error, unknown):
	
	reco = NB_IM - unknown
	errorRate = error * 100 / reco
	print "\n\n-------- RESULTS -------"
	print "Dataset is composed of : %d images \nErrors : %d" %(NB_IM, error)
	print "Non recognized images : %d \nError rate (without unrecognized) : %f%%\n\n" % (unknown, errorRate)


def main():
	
	#Make dataset
	alldata = ClassificationDataSet(TAILLE*TAILLE, 1, nb_classes=2, class_labels=['Meije', 'Cervin'])
	loadImCervin(alldata)
	loadImMeije(alldata)
	tstdata, trndata = alldata.splitWithProportion( 0.25 ) #75% for training, 25% for testing
	trndata._convertToOneOfMany( )
	tstdata._convertToOneOfMany( )

	print "Number of training patterns: ", len(trndata)
	print "Input and output dimensions: ", trndata.indim, trndata.outdim
	print "First sample (input, target, class):"
	print trndata['input'][0], trndata['target'][0], trndata['class'][0]

	#Define network(dimension of input layer, dimension of first hiddenlayer, dimension of output layer)	
	fnn = buildNetwork(trndata.indim, 500, 500, trndata.outdim, outclass=SoftmaxLayer)
	#fnn = NetworkReader.readFrom('network.xml') 
	trainer = BackpropTrainer(fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01) #define trainer
	
	#FIRST TRAINING METHOD
	for i in range(5):
		trainer.trainEpochs( 5 )
		trnresult = percentError( trainer.testOnClassData(), trndata['class'] )
		tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
		print "epoch: %4d" % trainer.totalepochs, "  train error: %5.2f%%" % trnresult, "  test error: %5.2f%%" % tstresult
	
	#SECOND TRAINING METHOD
	# trainer.trainUntilConvergence()
	# trnresult = percentError( trainer.testOnClassData(), trndata['class'] )
	# tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
	# print "  train error: %5.2f%%" % trnresult, "  test error: %5.2f%%" % tstresult
	
	#THIRD TRAINING METHOD
	# for i in range(5):
	# 	trainer.train()
	# 	cv = CrossValidator(trainer,trndata, n_folds=5, valfunc=ModuleValidator.MSE) #Returns the mean squared error 
	# 	print "MSE %f @ %i" %( cv.validate(), i )

	#trainer.testOnData(alldata, verbose= True)
	#NetworkWriter.writeToFile(fnn, 'network.xml')
	return (fnn,alldata)

if __name__ == '__main__':
	
	#Initialization
	error = 0
	unknown = 0

    	(fnn,alldata) = main()
	
	#Test for one image
	(result,imgName,imgClass) = recognizeOneIm(fnn,"mc1","MC",error,unknown)
	(error, unknown) = testOfValidity(result,imgName,imgClass,error,unknown)

	#Test for all dataset
	(error, unknown) = recognizeAllIm(fnn, alldata, error, unknown)
	statistics(error, unknown)

